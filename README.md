# krab robot

Resources for a larger Klann linkage based crab/spider robot.

Krab is driven by two brushless motors, powered by a 24V [ODrive controller](https://odriverobotics.com/).

The controller is a [Raspberry Pi 3B](https://www.raspberrypi.org/) running Raspbian with [ROS Noetic](https://www.raspberrypi.org/)

More design files coming soon!

![](krab.jpg)

Based on my previous klann bot, scaled 5x larger.
https://www.youtube.com/watch?v=skwF7IdNPIw
[![Son of Crabby](https://img.youtube.com/vi/skwF7IdNPIw/0.jpg)](https://www.youtube.com/watch?v=skwF7IdNPIw)

Which was in turn based on the hard work of jarkman:
https://code.google.com/archive/p/theeyestheywalk/

and Klann Research and Development...
http://mechanicalspider.com/

All parts CNC made with CamBam 
https://cambamcnc.com